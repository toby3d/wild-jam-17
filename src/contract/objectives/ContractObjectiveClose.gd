extends ContractObjective
class_name ContractObjectiveClose

var target: Contract

func connect_signals() -> void:
	var closable_contracts: Array = ContractSystem.get_closable_contracts()
	closable_contracts.shuffle()
	target = closable_contracts[0]
	# warning-ignore:return_value_discarded
	target.connect("completed", self, "_on_Contract_completed")
	# warning-ignore:return_value_discarded
	target.connect("failed", self, "_on_Contract_failed")


func _on_Contract_completed() -> void:
	if completed or failed:
		return
	finish()


func _on_Contract_failed() -> void:
	if completed or failed:
		return
	abort()


func as_text() -> String:
	return "Complete %s contract %s" % [(target as Contract).title, "(completed)" if completed else ""]
