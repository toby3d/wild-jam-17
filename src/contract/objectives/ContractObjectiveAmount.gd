extends ContractObjective
class_name ContractObjectiveAmount

export var target: int

func connect_signals(inventory: Inventory) -> void:
	target += inventory.coins
	# warning-ignore:return_value_discarded
	inventory.connect("coins_count_changes", self, "_on_Button_pressed")


func _on_Button_pressed(amount: int) -> void:
	if completed or failed:
		return
	emit_signal("updated", self)
	if completed or failed or amount < target:
		return
	finish()


func as_text() -> String:
	return "Collect %d coins %s" % [target, "(completed)" if completed else ""]
