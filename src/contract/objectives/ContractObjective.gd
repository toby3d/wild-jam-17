extends Node
class_name ContractObjective

signal completed(objective)
# warning-ignore:unused_signal
signal updated(objective)
signal failed(objective)

var completed: bool = false
var failed: bool = false

func finish() -> void:
	self.completed = true
	emit_signal("completed", self)


func abort() -> void:
	self.failed = true
	emit_signal("failed", self)


func as_text() -> String:
	return ""
