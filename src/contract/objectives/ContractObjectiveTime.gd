extends ContractObjective
class_name ContractObjectiveTime

signal timeout(before_timeout)

export var period: float = 0.0
export var complete_before_timeout: bool = false

onready var timer: Timer = $Timer

func _on_Contract_started() -> void:
	timer.start(period)


func _on_Objective_failed(_objective: ContractObjective) -> void:
	if completed or failed:
		return
	timer.stop()
	abort()


func _on_Objective_completed(_objective: ContractObjective) -> void:
	if completed or failed or !complete_before_timeout:
		return
	timer.stop()
	finish()


func _on_Timer_timeout() -> void:
	if completed or failed:
		return
	emit_signal("timeout", complete_before_timeout)
	if complete_before_timeout:
		abort()
		return
	finish()


func as_text() -> String:
	if complete_before_timeout:
		return "Complete other objectives before time is out"
	return "Follow other objectives %d seconds" % period
