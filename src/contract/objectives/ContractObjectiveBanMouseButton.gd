extends ContractObjective
class_name ContractObjectiveBanMouseButton

enum MouseButtons {NONE, BUTTON_LEFT, BUTTON_RIGHT}
export(MouseButtons) var button_index = BUTTON_LEFT

func _ready() -> void:
	set_process_input(false)


func _on_Contract_started() -> void:
	set_process_input(true)


func _input(event: InputEvent) -> void:
	if completed or failed or not event is InputEventMouseButton:
		return
	var mouse_event: = event as InputEventMouseButton
	if not mouse_event.pressed or mouse_event.button_index != button_index:
		return
	set_process_input(false)
	abort()


func _on_Timer_timeout(_before_timeout: bool) -> void:
	if completed or failed:
		return
	set_process_input(false)
	finish()


func as_text() -> String:
	var btn: String
	match button_index:
		MouseButtons.BUTTON_LEFT:
			btn = "left mouse button"
		MouseButtons.BUTTON_RIGHT:
			btn = "rignt mouse button"
	return "Don't use %s %s" % [btn, "(completed)" if completed else ""]
