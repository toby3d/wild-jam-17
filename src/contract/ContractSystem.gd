extends Node

onready var available_contracts = $Available
onready var active_contracts = $Active
onready var completed_contracts = $Completed
onready var failed_contracts = $Failed

var player
var game

func connect_signals(_game, _player) -> void:
	_game.connect("game_over", self, "_on_Game_game_over")
	player = _player
	game = _game


func find_available(reference: Contract) -> Contract:
	return available_contracts.find(reference)


func get_available_contracts() -> Array:
	return available_contracts.get_contracts()


func get_active_contracts() -> Array:
	return active_contracts.get_contracts()


func get_closable_contracts() -> Array:
	var contracts: Array = []
	for contract in get_active_contracts() as Array:
		if not contract.is_in_group("closable_contracts"):
			continue
		contracts.append(contract)
	return contracts


func is_available(reference: Contract) -> bool:
	return available_contracts.find(reference) != null


func start(reference: Contract) -> void:
	var contract: Contract = available_contracts.find(reference)
	for objective in contract.get_objectives():
		if objective as ContractObjectiveAmount:
			objective.connect_signals(player.inventory)

		if objective as ContractObjectiveClose:
			(objective as ContractObjectiveClose).connect_signals()

		if objective as ContractObjectiveTime or objective as ContractObjectiveBanMouseButton:
			pass

	# warning-ignore:return_value_discarded
	contract.connect("completed", self, "_on_Contract_completed", [contract])
	contract.connect("completed", game.get_node("AudioPlayer"), "_on_Contract_completed")
	contract.connect("failed", game.get_node("AudioPlayer"), "_on_Contract_failed")
	# warning-ignore:return_value_discarded
	contract.connect("failed", self, "_on_Contract_failed", [contract])
	available_contracts.remove_child(contract)
	active_contracts.add_child(contract)
	contract._start()


func _on_Contract_completed(contract: Contract) -> void:
	for reward in contract.get_rewards() as Array:
		player.inventory.add(reward.item, reward.amount)
	player.inventory.add_coins(contract.coins)
	active_contracts.remove_child(contract)
	completed_contracts.add_child(contract)


func _on_Contract_failed(contract: Contract) -> void:
	# warning-ignore:integer_division
	player.inventory.remove_coins(contract.coins / 2)
	active_contracts.remove_child(contract)
	failed_contracts.add_child(contract)


func _on_Game_game_over() -> void:
	pass
