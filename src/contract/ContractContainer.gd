extends Node

func find(_contract: Contract) -> Contract:
	for contract in get_contracts():
		if contract.name != _contract.name:
			continue
		return contract
	# warning-ignore:unreachable_code
	return null


func get_contracts() -> Array:
	return get_children()
