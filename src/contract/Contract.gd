extends Node
class_name Contract

signal started()
signal completed()
signal failed()

onready var objectives: = $Objectives
onready var rewards: = $Rewards

export var title: String = ""
export var description: String = ""
export var coins: int = 0

func _start() -> void:
	for objective in get_objectives() as Array:
		var _objective: = objective as ContractObjective
		_objective.completed = false
		_objective.failed = false
		# warning-ignore:return_value_discarded
		_objective.connect("completed", self, "_on_Objective_completed")
		# warning-ignore:return_value_discarded
		_objective.connect("failed", self, "_on_Objective_failed")
	emit_signal("started")


func get_objectives() -> Array:
	return objectives.get_children()


func get_completed_objectives() -> Array:
	var completed: Array = []
	for objective in get_objectives():
		if not objective.completed:
			continue
		completed.append(objective)
	return completed


func get_rewards() -> Array:
	return rewards.get_children()


func as_text() -> String:
	var text: String = description
	for objective in get_objectives():
		text += "\n* " + objective.as_text()
	return text


func _on_Objective_completed(_objective: ContractObjective) -> void:
	if get_completed_objectives().size() != get_objectives().size():
		return
	emit_signal("completed")


func _on_Objective_failed(_objective: ContractObjective) -> void:
	emit_signal("failed")
