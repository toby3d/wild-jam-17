extends Node

onready var music: = $Music as AudioStreamPlayer
onready var mission_completed: = $MissionCompleted as AudioStreamPlayer
onready var mission_failed: = $MissionFailed as AudioStreamPlayer

func _ready() -> void:
	music.play()


func _on_Game_started() -> void:
	music.bus = "Master"


func _on_Game_game_over() -> void:
	music.bus = "GameOver"


func _on_Game_victory() -> void:
	music.stop()


func _on_Contract_completed() -> void:
	mission_completed.play()


func _on_Contract_failed() -> void:
	mission_failed.play()
