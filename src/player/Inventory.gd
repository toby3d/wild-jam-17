extends Reference
class_name Inventory

signal item_count_changed(item, amount)
signal coins_count_changes(amount)

var coins: int
var content: Dictionary

func add(item: Item, amount: int = 1) -> void:
	if item in content:
		content[item] += amount
		return
	content[item] = 1 if item.unique else amount


func remove(item: Item, amount: int = 1):
	assert(item in content)
	assert(amount <= content[item])
	content[item] -= amount
	if content[item] == 0:
		# warning-ignore:return_value_discarded
		content.erase(item)
		emit_signal("item_count_changed", item, 0)
		return
	emit_signal("item_count_changed", item, content[item])


func get_items() -> Array:
	var items: Array = []
	for item in content.keys() as Array:
		items.append(item)
	return items


func get_unique_items() -> Array:
	var items: Array = []
	for item in content.keys() as Array:
		if not (item as Item).unique:
			continue
		items.append(item)
	return items


func add_coins(amount: int) -> void:
	coins += amount
	emit_signal("coins_count_changes", coins)


func remove_coins(amount: int) -> void:
	# warning-ignore:narrowing_conversion
	coins = max(0, coins - amount)
	emit_signal("coins_count_changes", coins)
