extends Control

onready var number_mantissa: Label = $Mantissa
onready var number_decimal: Label = $MarginContainer/Decimal

var timer: Timer

func _process(_delta: float) -> void:
	# NOTE(toby3d): I'm bad in math
	var s: float = timer.time_left / timer.wait_time
	var decimal: String = "%f.2" % timer.time_left
	number_mantissa.text = "%d." % timer.time_left
	number_decimal.text = decimal.split(".")[1].substr(0, 2)
	self.modulate = self.modulate.from_hsv(0, 1.0 - s, 1, 1.0)
