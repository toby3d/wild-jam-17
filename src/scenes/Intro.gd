extends Control

func _on_Link_pressed() -> void:
	OS.shell_open("www.toby3d.me")


func _on_Begin_pressed() -> void:
	get_tree().change_scene("res://src/scenes/Game.tscn")


func _on_Quit_pressed() -> void:
	get_tree().quit()
