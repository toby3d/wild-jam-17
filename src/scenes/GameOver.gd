extends CanvasLayer
class_name GameOver

signal restart_requested()

onready var sound: = $Sound as AudioStreamPlayer
onready var panel: = $Background as Panel
onready var message: = $Background/VBoxContainer/Message as Label
onready var options: = $Background/VBoxContainer/Options
onready var button_try_again: = options.get_node("TryAgain") as Button

enum Reason { FIRST_TRY, SECOND_CHANCE, LAST_CHANCE, GAME_OVER, VICTORY }

const MESSAGES = {
	Reason.FIRST_TRY: "Oh, it seems someone could not cope with their debt. It's okay, I'm ready to give you a second chance: sign another contract with me and, if you fulfill it, so be it, I will help you with your general debt.",
	Reason.SECOND_CHANCE: "This is bad luck. Well, you know my conditions: a new contract in return for help with debts. But my services are not endless: if more than 3 contracts are listed for you, then I will not be able to help you.",
	Reason.LAST_CHANCE: "This is my last suggestion. If you do not cope with those debts that are already attributable to you, then we will no longer have anything to talk about with you.",
	Reason.GAME_OVER: "Well, you can't get it all at once. Especially being in jail.",
	Reason.VICTORY: "It's nice to deal with you. But from now on, do not get into debt. Next time you might not be so lucky."
}

func hide() -> void:
	panel.hide()


func display(reason) -> void:
	if reason != Reason.VICTORY:
		sound.play()
	match reason:
		Reason.GAME_OVER, Reason.VICTORY:
			button_try_again.hide()
		_:
			pass
	message.text = MESSAGES[reason]
	panel.show()


func _on_TryAgain_pressed() -> void:
	emit_signal("restart_requested")


func _on_Exit_pressed() -> void:
	get_tree().quit()
