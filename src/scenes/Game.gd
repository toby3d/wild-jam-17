extends Node

signal started()
signal victory()
signal game_over()

export var session_time: float = 10.0

onready var player: = $Player as Player
onready var button: = $Container/Container/Button as Button
onready var timer: = $Timer as Timer
onready var audio_player: = $AudioPlayer as AudioStreamPlayer
onready var game_over: = $GameOver as GameOver
onready var gui: = $GUI as GUI

var general_contract: Contract

func _ready() -> void:
	general_contract = ContractSystem.get_available_contracts()[0]
	$Container/Timer.timer = timer
	ContractSystem.connect_signals(self, player)
	gui.connect_signals(player)
	ContractSystem.start(general_contract)
	# warning-ignore:return_value_discarded
	general_contract.connect("completed", self, "_on_Game_victory")
	emit_signal("started")


func _on_Button_pressed() -> void:
	player.inventory.add_coins(1)

	for item in player.inventory.get_items() as Array:
		match (item as Resource).resource_name:
			"autoclicker":
				player.inventory.add_coins(1)

	for item in player.inventory.get_unique_items() as Array:
		match (item as Resource).resource_name:
			"slowmo":
				timer.start(timer.time_left + 0.05)


func _on_Timer_timeout() -> void:
	timer.stop()
	emit_signal("game_over")


func _on_Game_started() -> void:
	game_over.hide()
	timer.start(session_time)


func _on_Game_victory() -> void:
	timer.stop()
	game_over.display(GameOver.Reason.VICTORY)


func _on_Game_game_over() -> void:
	var reason = GameOver.Reason.SECOND_CHANCE

	if ContractSystem.completed_contracts.get_children().size() == 0 and ContractSystem.failed_contracts.get_children().size() == 0:
		reason = GameOver.Reason.FIRST_TRY

	if ContractSystem.get_active_contracts().size() == 3:
		reason = GameOver.Reason.LAST_CHANCE

	if ContractSystem.get_active_contracts().size() > 3:
		reason = GameOver.Reason.GAME_OVER

	game_over.display(reason)


func _on_GameOver_restart_requested() -> void:
	var available_contracts: Array = ContractSystem.get_available_contracts()
	available_contracts.shuffle()
	var next_contract: Contract = available_contracts[0]

	while not _is_valid_closable(next_contract):
		available_contracts.shuffle()
		next_contract = available_contracts[0]

	ContractSystem.start(next_contract)
	emit_signal("started")


func _is_valid_closable(contract: Contract) -> bool:
	var closable_contracts: Array = ContractSystem.get_closable_contracts()
	for objective in contract.get_objectives() as Array:
		if closable_contracts.size() == 0 and objective is ContractObjectiveClose:
			return false
	return true
