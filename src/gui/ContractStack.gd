extends HBoxContainer
class_name ContractStack

signal updated()

const contract_card: = preload("res://src/gui/ContractCard.tscn")

func _ready() -> void:
	for contract in ContractSystem.get_available_contracts() as Array:
		contract.connect("started", self, "_on_Contract_started", [contract])
		contract.connect("completed", self, "_on_Contract_completed", [contract])
		contract.connect("failed", self, "_on_Contract_failed", [contract])


func _on_Contract_started(contract: Contract) -> void:
	var card: = contract_card.instance() as ContractCard
	add_child(card)
	for objective in contract.get_objectives() as Array:
		objective.connect("updated", self, "_on_Objective_updated")
		objective.connect("completed", self, "_on_Objective_completed")
		objective.connect("failed", self, "_on_Objective_failed")
	card.connect_signals(contract)
	card.set_meta("contract", contract)
	emit_signal("updated")


func _on_Contract_completed(contract: Contract) -> void:
	var card = _find_contract(contract)
	if card == null:
		return
	remove_child(card)
	emit_signal("updated")


func _on_Contract_failed(contract: Contract) -> void:
	var card = _find_contract(contract)
	if card == null:
		return
	remove_child(card)
	emit_signal("updated")


func _on_Objective_updated(objective: ContractObjective) -> void:
	var _objective: ContractObjective = _find_objective(objective)
	if _objective == null:
		return
	_objective = objective


func _on_Objective_completed(contract: Contract) -> void:
	var _contract: Contract = _find_contract(contract)
	if _contract == null:
		return
	remove_child(_contract)
	emit_signal("updated")


func _on_Objective_failed(contract: Contract) -> void:
	var _contract: Contract = _find_contract(contract)
	if _contract == null:
		return
	remove_child(_contract)
	emit_signal("updated")


func _find_contract(contract: Contract) -> Contract:
	for card in self.get_children() as Array:
		if card.get_meta("contract") != contract:
			continue
		return card
	# warning-ignore:unreachable_code
	return null


func _find_objective(objective: ContractObjective) -> ContractObjective:
	for card in get_children() as Array:
		var contract = card.get_meta("contract")
		if contract == null:
			continue
		for _objective in contract.get_objectives() as Array:
			if _objective != objective:
				continue
			return _objective
	# warning-ignore:unreachable_code
	return null
