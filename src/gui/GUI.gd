extends CanvasLayer
class_name GUI

onready var container: = $Container
onready var coins: = $Container/MarginContainer/Coins

func connect_signals(player: Player) -> void:
	# warning-ignore:return_value_discarded
	player.inventory.connect("coins_count_changes", self, "_on_Coins_changed")

func hide() -> void:
	container.hide()


func show() -> void:
	container.show()


func _on_Coins_changed(amount: int) -> void:
	coins.text = "Coins: %d" % amount
