extends Control
class_name ContractCard

onready var title = $Panel/MarginContainer/Container/Title
onready var description = $Panel/MarginContainer/Container/Description

func connect_signals(contract: Contract) -> void:
	title.text = "%s (%d coins)" % [contract.title, contract.coins]
	description.text = contract.as_text()


func _on_Objective_updated(_objective: ContractObjective) -> void:
	pass


func _on_Objective_completed() -> void:
	pass
